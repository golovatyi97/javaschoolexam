package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class PyramidBuilder extends CannotBuildPyramidException{

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param in to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
 public static int[][] buildPyramid(List<Integer> input) {
        // TODO : Implement your solution here
        int[][] pyramid;
        int[] dimension = sizeIsAppropriate(input);
        if(dimension == null) {
            throw new CannotBuildPyramidException();
        } else {
            pyramid = new int[dimension[0]][dimension[1]]; //raw/col
        }
        
        try{
            Collections.sort(input);
        } catch(Exception e) {
            throw new CannotBuildPyramidException();
        }
        int initSpace = dimension[1]/2;
        
        
        List<Integer> in = new ArrayList<>();
        for(int el: input) {
            in.add(el);
        }
        //проход по строкам
        for (int raw = 0; raw < dimension[0]; raw++) {
            //нули
            for (int i = 0; i < initSpace; i++) {
                pyramid[raw][i] = 0;
            }
            //числа
            for (int i = initSpace; i < dimension[1]-initSpace; i+=2) {
                try {
                    pyramid[raw][i] = in.remove(0);
                } catch(Exception e) { 
                    throw new CannotBuildPyramidException();
                }
            }            
            //нули
            for (int i = dimension[1] - initSpace; i < dimension[1] -1; i++) {
                pyramid[raw][i] = 0;
            }
            --initSpace;
        }
        
        
        return pyramid;
    }

    public static int progression(int n) { //1, 3, 6, 10 ...
        return n*(2+n-1)/2;
    }

    /**
     * Checks whether the size of list is appropriate
     * @return dimension of array as int[raw, col] or null.
     * 
     *
     * @param in to be used in the pyramid
     */
    public static int[] sizeIsAppropriate(List<Integer> in){
        int n = 1;
        int appropriatesize = progression(n);
        
        //in.l - 1 - 2 - 3 ... while i>0
        //далее сравниваем
        int comp = in.size();
        while (comp>0) {
            comp -= n;
            n++;
        }       
        if (comp==0) {
            return new int[]{n-1, 2*n-3}; 
        }
        return null;
    }
}
