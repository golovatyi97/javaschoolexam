package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.lang.*;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;



public class Calculator {
    HashSet<String> SIGNS;
    HashSet<String> BR;
    final String re = "[+,\\-,*,\\/]";
    public String evaluate(String s){
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        
        
        init();
        if(isValid(s)) {
            try{
                if(engine.eval(s).toString().equals("Infinity")){
                    return null;
                }
                return engine.eval(s).toString();
            } catch (Exception e) {

                System.out.println(e);
                return null;
            }
        } else {
            return null;
        }
    }
    
    boolean isValid(String s) {
        try{
            if (s.length()==0||s.contains(",")) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        
        int counter = Integer.MIN_VALUE;
        for(String tmp: s.split("")){
            try{
                if(tmp.equals("(")) {counter++;}
                else if(tmp.equals(")")) {counter--;}
            }
            catch(Exception e) {
                return false;
            }
            finally {
                
            }
        }
        if(counter!=Integer.MIN_VALUE) {
            return false;
        }
        
        s = s.replaceAll("[(,)]", "");
        String[] nums = s.split(re);
        for (String num:nums) {
            try {
                Double.parseDouble(num);
            } catch(Exception e) {
                return false;
            }
        }
        return true;
    }
    
    void init(){
        SIGNS = new HashSet<>();
        BR = new HashSet<>();
        if (SIGNS.isEmpty()) {
            SIGNS.addAll(Arrays.asList(new String[]{"+","-","*","/"}));
            BR.addAll(Arrays.asList(new String[]{"(",")"}));
        }
    }
}


