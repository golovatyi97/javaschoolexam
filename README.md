# README #

This is a repo with T-Systems Java School preliminary examination tasks.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Alexander Golovatyi
* Codeship : [ ![Codeship Status for golovatyi97/javaschoolexam](https://app.codeship.com/projects/b0e1f380-4e5a-0136-d634-72140786b795/status?branch=master)](https://app.codeship.com/projects/293465)

